<%--
  Created by IntelliJ IDEA.
  User: airat
  Date: 30.10.2017
  Time: 18:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file ="/WEB-INF/set-cookie.jsp"%>

<html>
<head>
    <title>Profile</title>
</head>
<body>
<%

    Object name = session.getAttribute("uname");
    if (name==null){
        response.sendRedirect("login.jsp");
    }

    out.println("<center><h1>Welcome: " + name + "</h1>");
    out.println("<br/><b>You are successfully logged in");
    out.println("<br/> memento = "+session.getAttribute("memento")+"<br/>");
    out.println("Current context:"+request.getContextPath()+"<br/>");
%>

<jsp:include page="/WEB-INF/show-cookies.jsp"/>

<%@ include file="/WEB-INF/clear-cookies.jsp"%>

<br/>
<a href="logout.jsp">Log out</a>
</body>
</html>
