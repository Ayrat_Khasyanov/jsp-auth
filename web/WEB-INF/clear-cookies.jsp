<%--
  Created by IntelliJ IDEA.
  User: airat
  Date: 31.10.2017
  Time: 19:21
  To change this template use File | Settings | File Templates.

--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(request.getParameter("removeCookie") != null){
        Cookie[] cookies = request.getCookies();
        Cookie ck;
        for(int i=0;i<cookies.length;i++){
            ck=cookies[i];
            if(!ck.getName().equals("JSESSIONID")){
                ck.setMaxAge(0);
                ck.setValue(null);
                response.addCookie(ck);
            }
        }
        session.removeAttribute("memento");
        out.println("<br/> <b> Cookies removed from this site");
    }
%>
<form action="#" method="get">
    <input type="submit" id="removeCookie" name="removeCookie" value="Remove cookie" >
</form>