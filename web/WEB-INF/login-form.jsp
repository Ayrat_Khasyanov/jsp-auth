<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Cookie[] cookies = request.getCookies();

    String name=null;
    int i = 0;
    if (cookies!=null) {
        while (i < cookies.length && !cookies[i].getName().equals("uname")) {
            i++;
        }
        if (i < cookies.length) {
            name = cookies[i].getValue();
        }
    }
%>
<form action='login.jsp' method="post">
    <input type='text' name='uname' value="<%= name!=null?name:"Your name" %>"/>
    <input type="password" name='upass' />
    <input type="checkbox" name='memento' value ='yes'/>
    <input type='submit' value='login' />
</body>