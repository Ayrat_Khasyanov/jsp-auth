<%--
  Created by IntelliJ IDEA.
  User: airat
  Date: 30.10.2017
  Time: 18:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    final String NAME = "test";
    final String PASS = "pass";

    String name = request.getParameter("uname");
    String pass = request.getParameter("upass");
%>
<html>
<%
    if (NAME.equals(name)&&PASS.equals(pass)){
        session.setAttribute("uname", name);
        session.setAttribute("memento", request.getParameter("memento"));
        response.sendRedirect("profile.jsp");
    } else {
%>
        <jsp:include page="/WEB-INF/login-form.jsp"/>
        <jsp:include page="/WEB-INF/show-cookies.jsp"/>
<%
    }
%>
</html>
